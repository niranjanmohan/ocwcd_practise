package com;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import DBO.addinfo;
import DBO.validate_info;

/**
 * Servlet implementation class controladd
 */
public class controladd extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public controladd() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		myFunction(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		myFunction(request,response);
		
		// TODO Auto-generated method stub
	}
	
	
	protected void myFunction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		addinfo obj = new addinfo();
		String cur_id=obj.getEmpid();
		String empid =(String)(request.getParameter("empid")!=null?request.getParameter("empid"):"0");		
		
		System.out.println(empid);
		HashMap<String,String> hmap = new HashMap<String,String>();
		
		if(!empid.equals("0"))
		{
			
		String empnm = (String)request.getParameter("empname");
		String gender = (String)request.getParameter("gender");
		String mstatus = (String)request.getParameter("mstatus");
		String dept = (String)request.getParameter("dept");
		String add1 = (String)request.getParameter("add1");
		String add2 = (String)request.getParameter("add2");
		String add3 = (String)request.getParameter("add3");
		String mobileno = (String)request.getParameter("mobileno");
		String lan = (String)request.getParameter("lanno");
		String email = (String)request.getParameter("email");
		
			
		obj.writefile(empid, empnm, gender, mstatus, dept, add1, add2, add3, mobileno, lan, email);
		request.setAttribute("args","admin" );
		
		//request.setAttribute("eid", cur_id);
		response.sendRedirect("http://localhost:8082/CheckEnv/controladd?empid=0");
		
		
		
		}
		else
		{
			validate_info er_msgs = new validate_info();
			
			hmap=er_msgs.check_inputs();
			request.setAttribute("errors", hmap);
			
			request.setAttribute("eid",cur_id);
			request.getRequestDispatcher("add.jsp").forward(request, response);
		}
		
		
	}
	}



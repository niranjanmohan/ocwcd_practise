package com;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import DBO.search;


/**
 * Servlet implementation class controldelete
 */
public class controldelete extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public controldelete() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{

		String ch =(String) (request.getParameter("opt")!=null?request.getParameter("opt"):"");
		if(ch.equals("id"))
		{
			String empid=(String) request.getParameter("empdetails");
			search obj =new search();
			if(obj.search_id(empid))
			{
				String op= obj.return_id(empid);
				request.setAttribute("msg",op );
				request.getRequestDispatcher("./delete.jsp").forward(request,response);
				
			}
			else
			{
				String op = ch+" :"+empid+" Not present in list";
				request.setAttribute("msg",op );
				request.getRequestDispatcher("./delete.jsp").forward(request,response);
			}
		}
		else if(ch.equals("name"))
		{
			String empnm = (String) request.getParameter("empdetails");
			search obj = new search();
			if(obj.search_name(empnm))
			{
				String op= obj.return_name(empnm);
				request.setAttribute("msg",op );
				request.getRequestDispatcher("./delete.jsp").forward(request,response);
				
			}
			else
			{
				
				String op = ch+" :"+ empnm +" Not present in list";
				request.setAttribute("msg",op );
				request.getRequestDispatcher("./delete.jsp").forward(request,response);
			}
			
		}
		else
		{
			String op = "select a choice first";
			request.setAttribute("msg",op );
			request.getRequestDispatcher("./delete.jsp").forward(request,response);
		}
		
		
	
		
		
		
	}
}

	
		
		
		
		
		
		// TODO Auto-generated method stub
	



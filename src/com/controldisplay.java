package com;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

/**
 * Servlet implementation class controlDisplay
 */
@WebServlet("/controlDisplay")
public class controldisplay extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public controldisplay() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("List", "List");
		
		DataSource ds = (DataSource)getServletContext().getAttribute("dsobj");
		String query ="select cid,statename from state ";
		try{
		Connection con = ds.getConnection();
		System.out.println("Connection obtined success");
		PreparedStatement userChk = con.prepareStatement(query);
		ResultSet rs = userChk.executeQuery();
		List<String>ls = new ArrayList<String>();
		while(rs.next())
		{
			System.out.println("values are"+rs.getString(1)+rs.getString(2));
			ls.add(rs.getString(1)+"~"+rs.getString(2));
		}
		request.setAttribute("cntList", ls);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		System.out.println("forwarding to list.jsp");
		request.getRequestDispatcher("./list.jsp").forward(request, response);
		
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request,response);
		// TODO Auto-generated method stub
	}

}

package listener;

import javax.naming.InitialContext;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import javax.sql.DataSource;

public class MyDBListener implements ServletContextListener{
	
	public void contextInitialized(ServletContextEvent event){
		System.out.println("******************Called listener******************");
		ServletContext sc = event.getServletContext();
		String db = sc.getInitParameter("dbcon");
		try{
		InitialContext ctx = new InitialContext();
		DataSource ds = (DataSource)ctx.lookup(db);
		sc.setAttribute("dsobj", ds);
		}
		catch(Exception e){
			e.printStackTrace();
		}
	
	}

	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	

}

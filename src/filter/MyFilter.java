package filter;

import java.io.IOException;
import java.util.Date;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

public class MyFilter implements Filter{
	private FilterConfig fc;
	public void init(FilterConfig config) throws ServletException{
		this.fc = config;
	}
	public void doFilter(ServletRequest req,ServletResponse res,FilterChain chain) throws IOException,ServletException{
		HttpServletRequest request = (HttpServletRequest)req;
		String rname = request.getRemoteUser();
		System.out.println("Printing inside the Filter class "+rname);
		fc.getServletContext().log("user"+rname+"updated:"+new Date());
		chain.doFilter(req, res);
	}
	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

}

package DB;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;


public class validate_info 
{
	String path="F:/niranjan/project/WebContent/WEB-INF/error.properties";
	public HashMap<String, String> check_inputs()
	{
		Properties prop = new Properties();
		FileInputStream f= null;
		HashMap<String,String> msgs= new HashMap<String,String>();
		
		try
		{
			f = new FileInputStream("F:/niranjan/project/WebContent/WEB-INF/error.properties");
			prop.load(f);
			String m1 = prop.getProperty("empty");
			String m2 = prop.getProperty("error_n");
			String m3 = prop.getProperty("error_add");
			String m4 = prop.getProperty("error_lan");
			String m5 = prop.getProperty("error_mob");
			String m6 = prop.getProperty("error_e");
			String m7 = prop.getProperty("error_m1");
			String m8 = prop.getProperty("error_l1");
			msgs.put("empty",m1);
			msgs.put("error_n", m2);
			msgs.put("error_add", m3);
			msgs.put("error_lan", m4);
			msgs.put("error_mob", m5);
			msgs.put("error_e", m6);
			msgs.put("error_m1", m7);
			msgs.put("error_l1", m8);
			
			
			
			f.close();
						
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch(IOException e1)
		{
			e1.printStackTrace();
		}
		catch(Exception m)
		{
			m.printStackTrace();
		}
		return msgs;
		
	}
	
	

}
